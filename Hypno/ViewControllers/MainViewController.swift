//
//  MainViewController.swift
//  Hypno
//
//  Created by Maxim Bakevich on 10/12/2021.
//

import UIKit

class MainViewController: UIPageViewController {
    let animations = AnimationProvider().loadAnimations()
    
    func getViewController(index: Int) -> AnimationViewController {
        var i = index
        if index > animations.count - 1 {
            i = 0
        } else if index < 0 {
            i = animations.count - 1
        }
        
        let anim = animations[i]
        let vc = AnimationViewController()
        vc.setup(animation: anim, index: i)
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let vc = getViewController(index: 0)
        
        setViewControllers([vc], direction: .forward, animated: true)
        
        self.dataSource = self
        self.delegate = self
        
        if !AppContext.accountService.welcomeTutorialShown {
            let tutorial = TutorialView(frame: view.frame)
            view.addSubview(tutorial)
            NSLayoutConstraint.activate([
                tutorial.topAnchor.constraint(equalTo: view.topAnchor),
                tutorial.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                tutorial.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                tutorial.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            
            let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
            let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
            
            leftSwipe.direction = .left
            rightSwipe.direction = .right

            tutorial.addGestureRecognizer(leftSwipe)
            tutorial.addGestureRecognizer(rightSwipe)
        }
    }
    
    @objc func handleSwipes(_ sender: UISwipeGestureRecognizer) {
        AppContext.accountService.welcomeTutorialShown = true
        sender.view?.removeFromSuperview()
    }
}

extension MainViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let animVC = viewController as? AnimationViewController else {
            return nil
        }
        
        return getViewController(index: animVC.index - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let animVC = viewController as? AnimationViewController else {
            return nil
        }
        
        return getViewController(index: animVC.index + 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        AccountCounter.numberOfShownAnimations += 1
        
        if AccountCounter.numberOfShownAnimations % 3 == 0 {
            _ = AppContext.adService.showInterstitial(viewController: self, placement: "default")
        }
    }
    
}
