//
//  AnimationViewController.swift
//  Hypno
//
//  Created by Maxim Bakevich on 10/12/2021.
//

import UIKit

class AnimationViewController: CommonViewController {
    public var index: Int = 0
    private var imageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .red
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func setupTabBarItem() {
        super.setupTabBarItem()
        
        shouldShowNavBar = false
    }
    
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imageView.startAnimating()
    }
    
    func setup(animation: Animation, index: Int) {
        self.index = index
        imageView.image = animation.frames.first
        imageView.animationImages = animation.frames
        imageView.animationDuration = animation.duration
    }
}
