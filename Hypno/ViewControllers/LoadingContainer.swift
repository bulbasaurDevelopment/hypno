//
//  LoadingContainer.swift
//  Hypno
//
//  Created by Maxim Bakevich on 09/12/2021.
//

import UIKit
import Lottie
import AppTrackingTransparency

class LoadingContainer: CommonViewController {
    var animationView: AnimationView = {
        let view = AnimationView(name: "loadingAnimation")
        view.loopMode = .loop
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func setupUI() {
        super.setupUI()
        
        view.addSubview(animationView)
        NSLayoutConstraint.activate([
            animationView.heightAnchor.constraint(equalToConstant: 20),
            animationView.widthAnchor.constraint(equalToConstant: 200),
            animationView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            animationView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50)
        ])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animationView.isHidden = false
        animationView.play()

        navigateToMain()
    }
    
    private func navigateToMain() {
        guard AppContext.accountService.ATTRequested else {
            showATTPermission {
                AppContext.accountService.ATTRequested = true
                DispatchQueue.main.async { [weak self] in
                    self?.navigateToMain()
                }
            }
            return
        }
        
        guard AppContext.accountService.GDPRAccepted else {
            showGDPRAlert()
            return
        }
        
        AppContext.adService.start()

        performSegue(withIdentifier: R.segue.loadingContainer.showMainSegue, sender: nil)
    }
}

extension LoadingContainer {
    private func showGDPRAlert() {
        let alert = UIAlertController(title: R.string.localizable.gdpr_alert_title(),
                                      message: R.string.localizable.gdpr_alert_body_text(),
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: R.string.localizable.gdpr_alert_read_button_title(),
                                      style: UIAlertAction.Style.default,
                                      handler: { [weak self] _ in
            AppContext.analyticsService.logEvent(CommonAppEvents.gdpr_read, placement: CommonAppPlacement.gdpr)
            self?.openLink(AppContext.privacyPolicyPath)
                                        self?.showGDPRAlert()
                                      }))
        alert.addAction(UIAlertAction(title: R.string.localizable.gdpr_alert_accept_button_title(),
                                      style: UIAlertAction.Style.default,
                                      handler: { [weak self] _ in
            AppContext.analyticsService.logEvent(.gdpr_accept, placement: .gdpr)
            AppContext.accountService.GDPRAccepted = true
            self?.navigateToMain()
                                      }))
        self.present(alert, animated: true, completion: nil)
        AppContext.analyticsService.logEvent(.gdpr_show, placement: .gdpr)
    }
    
    private func showATTPermission(block: @escaping (() -> Void)) {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                block()
            })
        } else {
            block()
        }
    }
}
