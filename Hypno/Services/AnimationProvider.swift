//
//  AnimationProvider.swift
//  Hypno
//
//  Created by Maxim Bakevich on 10/12/2021.
//

import Foundation
import UIKit

struct Animation {
    let duration: Double
    let frames: [UIImage]
    
    init(prefix: String, framesCount: Int) {
        var frames = [UIImage]()
        for i in 0...framesCount {
            let name = "\(prefix)-\(i).jpg"
            frames.append(UIImage(named: name)!)
        }
        self.frames = frames
        self.duration = 0.04 * Double(framesCount)
    }
}

class AnimationProvider {
    func loadAnimations() -> [Animation] {
        return [
            Animation(prefix: "anim-13", framesCount: 27),
            Animation(prefix: "anim-1", framesCount: 7),
            Animation(prefix: "anim-2", framesCount: 4),
            Animation(prefix: "anim-3", framesCount: 8),
            Animation(prefix: "anim-4", framesCount: 8),
            Animation(prefix: "anim-5", framesCount: 12),
            Animation(prefix: "anim-6", framesCount: 14),
            Animation(prefix: "anim-7", framesCount: 45),
            Animation(prefix: "anim-8", framesCount: 5),
            Animation(prefix: "anim-9", framesCount: 9),
            Animation(prefix: "anim-10", framesCount: 18),
            Animation(prefix: "anim-11", framesCount: 14),
            Animation(prefix: "anim-12", framesCount: 56)
        ]
    }
}
