//
//  AccountCounter.swift
//  Hypno
//
//  Created by Maxim Bakevich on 10/12/2021.
//

import Foundation

private let numberOfShownAnimationsKey = "numberOfShownAnimationsKey"

class AccountCounter {
    static public var numberOfShownAnimations: Int {
        set {
            UserDefaults.standard.set(newValue, forKey: numberOfShownAnimationsKey)
        }
        get {
            return UserDefaults.standard.integer(forKey: numberOfShownAnimationsKey)
        }
    }
}
