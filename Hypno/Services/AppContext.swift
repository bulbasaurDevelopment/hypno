//
//  AppContext.swift
//  Hypno
//
//  Created by Maxim Bakevich on 06/12/2021.
//

import Foundation
import Firebase

public struct AppContext {
    public static let appodealKey: String = "eed9b5838ea5ab097804909ea483fa3eba31b433ed652041"
    
    public static let privacyPolicyPath: String = "https://sites.google.com/view/callrecappitc/privacy-policy"
    public static let termsOfUsePath: String = "https://sites.google.com/view/callrecappitc/terms-of-use"
    
    public static let adService: AdService = AppodealAdapter(key: appodealKey, adTypes: [.interstitial])
    public static var accountService: AccountService = AccountManager()
    public static var analyticsService: AnalyticsService = FirebaseAdapter()
}
